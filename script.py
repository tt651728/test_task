import sys
import time

def write_to_file(argument):
    with open('output.txt', 'a') as file:
        file.write(argument + '\n')

if __name__ == '__main__':
    arguments = {
        'arg1': 'value1',
        'arg2': 'value2',
        'arg3': 'value3'
    }

    arg_keys = list(arguments.keys())
    current_arg_index = 0

    while True:
        current_arg = arg_keys[current_arg_index]
        write_to_file(arguments[current_arg])
        time.sleep(60)
        current_arg_index = (current_arg_index + 1) % len(arg_keys)
